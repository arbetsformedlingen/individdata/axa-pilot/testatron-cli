package com.arbetsformedlingen;

import com.jayway.jsonpath.JsonPath;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MiscUtilTest {
    @Test
    @Tag("Unit")
    public void construct() {
        new MiscUtil();
    }

    @Test
    @Tag("Unit")
    public void createEncryptedContent() throws JOSEException {
        // Arrange
        final RSAPrivateKey clientPrivateKey = mock(RSAPrivateKey.class);
        final RSAPublicKey clientPublicKey = mock(RSAPublicKey.class);

        final List<Map<String, String>> claims = List.of(Map.of("ex-claim-key", "ex-claim-value"));

        final JWTClaimsSet mockJWTClaimsSet = mock(JWTClaimsSet.class);
        final JWSHeader mockJWSHeader = mock(JWSHeader.class);

        final Date mockInnerNotBeforeTime = mock(Date.class);
        final Date mockInnerExpirationTime = mock(Date.class);
        final Date mockOuterNotBeforeTime = mock(Date.class);
        final Date mockOuterExpirationTime = mock(Date.class);

        final var constructionJWTClaimsSetBuilderArgs = new HashMap<JWTClaimsSet.Builder, List<?>>();
        final var constructionJWSHeaderBuilderArgs = new HashMap<JWSHeader.Builder, List<?>>();
        final var constructionSignedJWTArgs = new HashMap<SignedJWT, List<?>>();
        final var constructionRSASSASignerArgs = new HashMap<RSASSASigner, List<?>>();
        final var constructionJWEHeaderArgs = new HashMap<JWEHeader, List<?>>();
        final var constructionEncryptedJWTArgs = new HashMap<EncryptedJWT, List<?>>();
        final var constructionRSAEncrypterArgs = new HashMap<RSAEncrypter, List<?>>();

        try (MockedStatic<UUID> staticUUID = mockStatic(UUID.class);
             MockedStatic<Date> staticDate = mockStatic(Date.class);
             MockedConstruction<JWTClaimsSet.Builder> constructionJWTClaimsSetBuilder = mockConstruction(JWTClaimsSet.Builder.class, (mock, context) -> {
                 constructionJWTClaimsSetBuilderArgs.put(mock, context.arguments());
                 when(mock.jwtID(any())).thenReturn(mock);
                 when(mock.issuer(any())).thenReturn(mock);
                 when(mock.notBeforeTime(any())).thenReturn(mock);
                 when(mock.expirationTime(any())).thenReturn(mock);
                 when(mock.claim(any(), any())).thenReturn(mock);
                 when(mock.build()).thenReturn(mockJWTClaimsSet);
             });
             MockedConstruction<JWSHeader.Builder> constructionJWSHeaderBuilder = mockConstruction(JWSHeader.Builder.class, (mock, context) -> {
                 constructionJWSHeaderBuilderArgs.put(mock, context.arguments());
                 when(mock.build()).thenReturn(mockJWSHeader);
             });
             MockedConstruction<RSASSASigner> constructionRSASSASigner = mockConstruction(RSASSASigner.class, (mock, context) -> {
                 constructionRSASSASignerArgs.put(mock, context.arguments());
             });
             MockedConstruction<SignedJWT> constructionSignedJWT = mockConstruction(SignedJWT.class, (mock, context) -> {
                 constructionSignedJWTArgs.put(mock, context.arguments());
                 when(mock.serialize()).thenReturn("ex-inner-serialized");
             });
             MockedConstruction<JWEHeader> constructionJWEHeader = mockConstruction(JWEHeader.class, (mock, context) -> {
                 constructionJWEHeaderArgs.put(mock, context.arguments());
             });
             MockedConstruction<EncryptedJWT> constructionEncryptedJWT = mockConstruction(EncryptedJWT.class, (mock, context) -> {
                 constructionEncryptedJWTArgs.put(mock, context.arguments());
                 when(mock.serialize()).thenReturn("ex-outer-serialized");
             });
             MockedConstruction<RSAEncrypter> constructionRSAEncrypter = mockConstruction(RSAEncrypter.class, (mock, context) -> {
                 constructionRSAEncrypterArgs.put(mock, context.arguments());
             });) {
            final UUID mockUUID = mock(UUID.class);
            staticUUID.when(UUID::randomUUID).thenReturn(mockUUID);
            when(mockUUID.toString())
                    .thenReturn("ex-uuid1")
                    .thenReturn("ex-uuid2");

            staticDate.when(() -> Date.from(any()))
                    .thenReturn(mockInnerNotBeforeTime)
                    .thenReturn(mockInnerExpirationTime)
                    .thenReturn(mockOuterNotBeforeTime)
                    .thenReturn(mockOuterExpirationTime);

            // Act
            final String encryptedContent = MiscUtil.createEncryptedContent("ex-client-name", clientPrivateKey, clientPublicKey, claims);

            // Assert
            staticUUID.verify(UUID::randomUUID, times(2));
            staticDate.verify(() -> Date.from(any()), times(4));

            assertEquals(2, constructionJWTClaimsSetBuilder.constructed().size());

            final JWTClaimsSet.Builder mockJWTClaimsSetBuilder0 = constructionJWTClaimsSetBuilder.constructed().get(0);
            assertEquals(0, constructionJWTClaimsSetBuilderArgs.get(mockJWTClaimsSetBuilder0).size());
            verify(mockJWTClaimsSetBuilder0, times(1)).jwtID("ex-uuid1");
            verify(mockJWTClaimsSetBuilder0, times(1)).issuer("ex-client-name");
            verify(mockJWTClaimsSetBuilder0, times(1)).claim(any(), any());
            verify(mockJWTClaimsSetBuilder0, times(1)).claim("ex-claim-key", "ex-claim-value");
            verify(mockJWTClaimsSetBuilder0, times(1)).notBeforeTime(mockInnerNotBeforeTime);
            verify(mockJWTClaimsSetBuilder0, times(1)).expirationTime(mockInnerExpirationTime);
            verify(mockJWTClaimsSetBuilder0, times(1)).build();

            assertEquals(1, constructionJWSHeaderBuilder.constructed().size());

            final JWSHeader.Builder mockJWSHeaderBuilder = constructionJWSHeaderBuilder.constructed().get(0);
            assertEquals(1, constructionJWSHeaderBuilderArgs.get(mockJWSHeaderBuilder).size());
            assertEquals(JWSAlgorithm.RS256, constructionJWSHeaderBuilderArgs.get(mockJWSHeaderBuilder).get(0));

            assertEquals(1, constructionSignedJWT.constructed().size());

            final SignedJWT mockSignedJWT = constructionSignedJWT.constructed().get(0);
            assertEquals(2, constructionSignedJWTArgs.get(mockSignedJWT).size());
            assertEquals(mockJWSHeader, constructionSignedJWTArgs.get(mockSignedJWT).get(0));
            assertEquals(mockJWTClaimsSet, constructionSignedJWTArgs.get(mockSignedJWT).get(1));

            assertEquals(1, constructionRSASSASigner.constructed().size());

            final RSASSASigner mockRSASSASigner = constructionRSASSASigner.constructed().get(0);
            assertEquals(1, constructionRSASSASignerArgs.get(mockRSASSASigner).size());
            assertEquals(clientPrivateKey, constructionRSASSASignerArgs.get(mockRSASSASigner).get(0));

            final JWTClaimsSet.Builder mockJWTClaimsSetBuilder1 = constructionJWTClaimsSetBuilder.constructed().get(1);
            verify(mockJWTClaimsSetBuilder1, times(1)).jwtID("ex-uuid2");
            verify(mockJWTClaimsSetBuilder1, times(1)).issuer("ex-client-name");
            verify(mockJWTClaimsSetBuilder1, times(1)).claim(any(), any());
            verify(mockJWTClaimsSetBuilder1, times(1)).claim("payload", "ex-inner-serialized");
            verify(mockJWTClaimsSetBuilder1, times(1)).notBeforeTime(mockOuterNotBeforeTime);
            verify(mockJWTClaimsSetBuilder1, times(1)).expirationTime(mockOuterExpirationTime);
            verify(mockJWTClaimsSetBuilder1, times(1)).build();

            assertEquals(1, constructionJWEHeader.constructed().size());

            final JWEHeader mockJWEHeader = constructionJWEHeader.constructed().get(0);
            assertEquals(2, constructionJWEHeaderArgs.get(mockJWEHeader).size());
            assertEquals(JWEAlgorithm.RSA_OAEP_512, constructionJWEHeaderArgs.get(mockJWEHeader).get(0));
            assertEquals(EncryptionMethod.A128GCM, constructionJWEHeaderArgs.get(mockJWEHeader).get(1));

            assertEquals(1, constructionEncryptedJWT.constructed().size());

            final EncryptedJWT mockEncryptedJWT = constructionEncryptedJWT.constructed().get(0);
            assertEquals(2, constructionEncryptedJWTArgs.get(mockEncryptedJWT).size());
            assertEquals(mockJWEHeader, constructionEncryptedJWTArgs.get(mockEncryptedJWT).get(0));
            assertEquals(mockJWTClaimsSet, constructionEncryptedJWTArgs.get(mockEncryptedJWT).get(1));

            assertEquals(1, constructionRSAEncrypter.constructed().size());

            final RSAEncrypter mockRSAEncrypter = constructionRSAEncrypter.constructed().get(0);
            assertEquals(1, constructionRSAEncrypterArgs.get(mockRSAEncrypter).size());
            assertEquals(clientPublicKey, constructionRSAEncrypterArgs.get(mockRSAEncrypter).get(0));

            assertEquals("ex-outer-serialized", encryptedContent);
        }
    }

    @Test
    @Tag("Unit")
    public void getJWSClaimsSet_whenResponseBodyIsNull_returnNull() throws ParseException, JOSEException {
        // Arrange
        final var mockPrivateKey = mock(RSAPrivateKey.class);
        final var mockPublicKey = mock(RSAPublicKey.class);

        // Act
        final JWTClaimsSet jwsClaimsSet = MiscUtil.getJWSClaimsSet(null, mockPrivateKey, mockPublicKey);

        // Assert
        assertNull(jwsClaimsSet);
    }

    @Test
    @Tag("Unit")
    public void getJWSClaimsSet() throws ParseException, JOSEException {
        // Arrange
        final var mockResponseBody = "{ \"foo\": \"bar\" }";
        final var mockPrivateKey = mock(RSAPrivateKey.class);
        final var mockPublicKey = mock(RSAPublicKey.class);

        final var mockEncryptedJWT = mock(EncryptedJWT.class);

        final var mockJWEClaimsSet = mock(JWTClaimsSet.class);
        when(mockEncryptedJWT.getJWTClaimsSet()).thenReturn(mockJWEClaimsSet);

        when(mockJWEClaimsSet.getClaim("payload")).thenReturn("ex-payload");

        final var mockSignedJWT = mock(SignedJWT.class);

        when(mockSignedJWT.verify(any(RSASSAVerifier.class))).thenReturn(true);

        final var mockJWSClaimsSet = mock(JWTClaimsSet.class);
        when(mockSignedJWT.getJWTClaimsSet()).thenReturn(mockJWSClaimsSet);

        when(mockJWSClaimsSet.getClaims()).thenReturn(Map.of("ex-claim-key", "ex-claim-value"));

        final var constructionRSADecrypterArgs = new HashMap<RSADecrypter, List<?>>();
        final var constructionRSASSAVerifierArgs = new HashMap<RSASSAVerifier, List<?>>();

        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class);
             MockedStatic<EncryptedJWT> staticEncryptedJWT = mockStatic(EncryptedJWT.class);
             MockedConstruction<RSADecrypter> constructionRSADecrypter = mockConstruction(RSADecrypter.class, (mock, context) -> {
                 constructionRSADecrypterArgs.put(mock, context.arguments());
             });
             MockedStatic<SignedJWT> staticSignedJWT = mockStatic(SignedJWT.class);
             MockedConstruction<RSASSAVerifier> constructionRSASSAVerifier = mockConstruction(RSASSAVerifier.class, (mock, context) -> {
                 constructionRSASSAVerifierArgs.put(mock, context.arguments());
             })) {
            staticJsonPath.when(() -> JsonPath.read(mockResponseBody, "$.encryptedContent")).thenReturn("ex-encrypted-content");
            staticEncryptedJWT.when(() -> EncryptedJWT.parse("ex-encrypted-content")).thenReturn(mockEncryptedJWT);
            staticSignedJWT.when(() -> SignedJWT.parse("ex-payload")).thenReturn(mockSignedJWT);

            // Act
            final JWTClaimsSet jwsClaimsSet = MiscUtil.getJWSClaimsSet(mockResponseBody, mockPrivateKey, mockPublicKey);

            // Assert
            verify(mockEncryptedJWT, times(1)).decrypt(any(RSADecrypter.class));
            verify(mockEncryptedJWT, times(1)).getJWTClaimsSet();
            verify(mockJWEClaimsSet, times(1)).getClaim("payload");
            verify(mockSignedJWT, times(1)).verify(any(RSASSAVerifier.class));
            verify(mockSignedJWT, times(1)).getJWTClaimsSet();
            verify(mockJWSClaimsSet, times(1)).getClaims();

            assertEquals(1, constructionRSADecrypter.constructed().size());

            final var mockRSADecrypter = constructionRSADecrypter.constructed().get(0);
            assertEquals(mockPrivateKey, constructionRSADecrypterArgs.get(mockRSADecrypter).get(0));

            assertEquals(1, constructionRSASSAVerifier.constructed().size());

            final var mockRSASSAVerifier = constructionRSASSAVerifier.constructed().get(0);
            assertEquals(mockPublicKey, constructionRSASSAVerifierArgs.get(mockRSASSAVerifier).get(0));

            assertEquals(mockJWSClaimsSet, jwsClaimsSet);
        }
    }

    @Test
    @Tag("Unit")
    public void getJWSClaimsSet_whenSignatureVerificationFails_throwError() throws ParseException, JOSEException {
        // Arrange
        final var mockResponseBody = "{ \"foo\": \"bar\" }";
        final var mockPrivateKey = mock(RSAPrivateKey.class);
        final var mockPublicKey = mock(RSAPublicKey.class);

        final var mockEncryptedJWT = mock(EncryptedJWT.class);

        final var mockJWEClaimsSet = mock(JWTClaimsSet.class);
        when(mockEncryptedJWT.getJWTClaimsSet()).thenReturn(mockJWEClaimsSet);

        when(mockJWEClaimsSet.getClaim("payload")).thenReturn("ex-payload");

        final var mockSignedJWT = mock(SignedJWT.class);

        when(mockSignedJWT.verify(any(RSASSAVerifier.class))).thenReturn(false);

        final var mockJWSClaimsSet = mock(JWTClaimsSet.class);
        when(mockSignedJWT.getJWTClaimsSet()).thenReturn(mockJWSClaimsSet);

        when(mockJWSClaimsSet.getClaims()).thenReturn(Map.of("ex-claim-key", "ex-claim-value"));

        final var constructionRSADecrypterArgs = new HashMap<RSADecrypter, List<?>>();
        final var constructionRSASSAVerifierArgs = new HashMap<RSASSAVerifier, List<?>>();

        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class);
             MockedStatic<EncryptedJWT> staticEncryptedJWT = mockStatic(EncryptedJWT.class);
             MockedConstruction<RSADecrypter> constructionRSADecrypter = mockConstruction(RSADecrypter.class, (mock, context) -> {
                 constructionRSADecrypterArgs.put(mock, context.arguments());
             });
             MockedStatic<SignedJWT> staticSignedJWT = mockStatic(SignedJWT.class);
             MockedConstruction<RSASSAVerifier> constructionRSASSAVerifier = mockConstruction(RSASSAVerifier.class, (mock, context) -> {
                 constructionRSASSAVerifierArgs.put(mock, context.arguments());
             })) {
            staticJsonPath.when(() -> JsonPath.read(mockResponseBody, "$.encryptedContent")).thenReturn("ex-encrypted-content");
            staticEncryptedJWT.when(() -> EncryptedJWT.parse("ex-encrypted-content")).thenReturn(mockEncryptedJWT);
            staticSignedJWT.when(() -> SignedJWT.parse("ex-payload")).thenReturn(mockSignedJWT);

            // Act
            final var error = assertThrows(Error.class, () -> MiscUtil.getJWSClaimsSet(mockResponseBody, mockPrivateKey, mockPublicKey));

            // Assert
            verify(mockEncryptedJWT, times(1)).decrypt(any(RSADecrypter.class));
            verify(mockEncryptedJWT, times(1)).getJWTClaimsSet();
            verify(mockJWEClaimsSet, times(1)).getClaim("payload");
            verify(mockSignedJWT, times(1)).verify(any(RSASSAVerifier.class));
            verify(mockSignedJWT, times(0)).getJWTClaimsSet();
            verify(mockJWSClaimsSet, times(0)).getClaims();

            assertEquals(1, constructionRSADecrypter.constructed().size());

            final var mockRSADecrypter = constructionRSADecrypter.constructed().get(0);
            assertEquals(mockPrivateKey, constructionRSADecrypterArgs.get(mockRSADecrypter).get(0));

            assertEquals(1, constructionRSASSAVerifier.constructed().size());

            final var mockRSASSAVerifier = constructionRSASSAVerifier.constructed().get(0);
            assertEquals(mockPublicKey, constructionRSASSAVerifierArgs.get(mockRSASSAVerifier).get(0));

            assertEquals("Signature verification failed", error.getMessage());
        }
    }
}
