package com.arbetsformedlingen;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ValidationUtilTest {
    @Test
    @Tag("Unit")
    public void construct() {
        new ValidationUtil();
    }
    @Test
    @Tag("Unit")
    public void validateServiceResponse() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            staticJsonPath.when(() -> JsonPath.read(body, "$.clientId")).thenReturn("ex-client-id");
            staticJsonPath.when(() -> JsonPath.read(body, "$.name")).thenReturn("ex-client-name");
            staticJsonPath.when(() -> JsonPath.read(body, "$.updated")).thenReturn("2024-01-18");
            staticJsonPath.when(() -> JsonPath.read(body, "$.created")).thenReturn("2024-01-18");

            // Act
            ValidationUtil.validateServiceResponse(body, "ex-client-id", "ex-client-name");
        }
    }

    @Test
    @Tag("Unit")
    public void validateServiceResponse_whenClientIdDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            staticJsonPath.when(() -> JsonPath.read(body, "$.clientId")).thenReturn("ex-wrong-client-id");
            staticJsonPath.when(() -> JsonPath.read(body, "$.name")).thenReturn("ex-client-name");
            staticJsonPath.when(() -> JsonPath.read(body, "$.updated")).thenReturn("2024-01-18");
            staticJsonPath.when(() -> JsonPath.read(body, "$.created")).thenReturn("2024-01-18");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateServiceResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'clientId' does not match", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateServiceResponse_whenClientNameDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            staticJsonPath.when(() -> JsonPath.read(body, "$.clientId")).thenReturn("ex-client-id");
            staticJsonPath.when(() -> JsonPath.read(body, "$.name")).thenReturn("ex-wrong-client-name");
            staticJsonPath.when(() -> JsonPath.read(body, "$.updated")).thenReturn("2024-01-18");
            staticJsonPath.when(() -> JsonPath.read(body, "$.created")).thenReturn("2024-01-18");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateServiceResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'name' does not match", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateServiceResponse_whenUpdatedNull_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            staticJsonPath.when(() -> JsonPath.read(body, "$.clientId")).thenReturn("ex-client-id");
            staticJsonPath.when(() -> JsonPath.read(body, "$.name")).thenReturn("ex-client-name");
            staticJsonPath.when(() -> JsonPath.read(body, "$.updated")).thenReturn(null);
            staticJsonPath.when(() -> JsonPath.read(body, "$.created")).thenReturn("2024-01-18");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateServiceResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'updated' must not be null", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateServiceResponse_whenCreatedNull_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            staticJsonPath.when(() -> JsonPath.read(body, "$.clientId")).thenReturn("ex-client-id");
            staticJsonPath.when(() -> JsonPath.read(body, "$.name")).thenReturn("ex-client-name");
            staticJsonPath.when(() -> JsonPath.read(body, "$.updated")).thenReturn("2024-01-18");
            staticJsonPath.when(() -> JsonPath.read(body, "$.created")).thenReturn(null);

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateServiceResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'created' must not be null", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateConsentResponse() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-client-name");
            when(mockDocument.read("$.authCode")).thenReturn("ex-auth-code");

            // Act
            ValidationUtil.validateConsentResponse(body, "ex-client-id", "ex-client-name");
        }
    }

    @Test
    @Tag("Unit")
    public void validateConsentResponse_whenClientIdDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-wrong-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-client-name");
            when(mockDocument.read("$.authCode")).thenReturn("ex-auth-code");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateConsentResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'clientId' does not match", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateConsentResponse_whenClientNameDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-wrong-client-name");
            when(mockDocument.read("$.authCode")).thenReturn("ex-auth-code");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateConsentResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'name' does not match", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateConsentResponse_whenAuthCodeIsNull_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-client-name");
            when(mockDocument.read("$.authCode")).thenReturn(null);

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateConsentResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'authCode' must not be null", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateRevokeResponse() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-client-name");
            when(mockDocument.read("$.authCode")).thenReturn(null);

            // Act
            ValidationUtil.validateRevokeResponse(body, "ex-client-id", "ex-client-name");
        }
    }

    @Test
    @Tag("Unit")
    public void validateRevokeResponse_whenClientIdDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-wrong-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-client-name");
            when(mockDocument.read("$.authCode")).thenReturn(null);

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateRevokeResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'clientId' does not match", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateRevokeResponse_whenClientNameDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-wrong-client-name");
            when(mockDocument.read("$.authCode")).thenReturn(null);

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateRevokeResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'name' does not match", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateRevokeResponse_whenAuthCodeIsNotNull_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.clientId")).thenReturn("ex-client-id");
            when(mockDocument.read("$.name")).thenReturn("ex-client-name");
            when(mockDocument.read("$.authCode")).thenReturn("ex-auth-code");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateRevokeResponse(body, "ex-client-id", "ex-client-name"));

            // Assert
            assertEquals("Property 'authCode' must be null", thrown.getMessage());
        }
    }

    @Test
    @Tag("Unit")
    public void validateStatusResponse() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.status")).thenReturn("Unemployed");

            // Act
            ValidationUtil.validateStatusResponse(body);
        }
    }

    @Test
    @Tag("Unit")
    public void validateStatusResponse_whenStatusDoesNotMatch_throw() throws Exception {
        // Arrange
        try (MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class)) {
            final var body = "{\"foo\": \"bar\"}";
            final var mockDocument = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(body)).thenReturn(mockDocument);
            when(mockDocument.read("$.status")).thenReturn("ex-wrong-status");

            // Act
            final var thrown = assertThrows(Exception.class, () -> ValidationUtil.validateStatusResponse(body));

            // Assert
            assertEquals("Property 'status' does not match 'Unemployed'", thrown.getMessage());
        }
    }
}
