package com.arbetsformedlingen;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class RequestFactoryTest {
    @Test
    @Tag("Unit")
    public void construct() {
        new RequestFactory();
    }

    @Test
    @Tag("Unit")
    public void initRequest() throws Exception {
        // Arrange
        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class)) {
            staticMiscUtil.when(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "clientId", "ex-client-id",
                                    "clientSecret", "ex-client-secret",
                                    "loginId", "ex-foreign-id",
                                    "personalIdentityNumber", "ex-pnr"
                            )
                    )
            )).thenReturn("ex-encrypted-content");

            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/init",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            )).thenReturn("ex-response");

            // Act
            final var response = RequestFactory.initRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-foreign-id",
                    "ex-pnr");

            // Assert
            staticMiscUtil.verify(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "clientId", "ex-client-id",
                                    "clientSecret", "ex-client-secret",
                                    "loginId", "ex-foreign-id",
                                    "personalIdentityNumber", "ex-pnr"
                            )
                    )
            ), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/init",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            ), times(1));

            assertEquals("ex-response", response);
        }
    }

    @Test
    @Tag("Unit")
    public void tokenRequest() throws Exception {
        // Arrange
        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class)) {
            staticMiscUtil.when(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "clientId", "ex-client-id",
                                    "clientSecret", "ex-client-secret",
                                    "authCode", "ex-auth-code"
                            )
                    )
            )).thenReturn("ex-encrypted-content");

            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/token",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            )).thenReturn("ex-response");

            // Act
            final var response = RequestFactory.tokenRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-auth-code");

            // Assert
            staticMiscUtil.verify(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "clientId", "ex-client-id",
                                    "clientSecret", "ex-client-secret",
                                    "authCode", "ex-auth-code"
                            )
                    )
            ), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/token",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            ), times(1));

            assertEquals("ex-response", response);
        }
    }

    @Test
    @Tag("Unit")
    public void refreshRequest() throws Exception {
        // Arrange
        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class)) {
            staticMiscUtil.when(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "clientId", "ex-client-id",
                                    "clientSecret", "ex-client-secret",
                                    "refreshToken", "ex-refresh-token"
                            )
                    )
            )).thenReturn("ex-encrypted-content");

            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/refresh",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            )).thenReturn("ex-response");

            // Act
            final var response = RequestFactory.refreshRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-refresh-token");

            // Assert
            staticMiscUtil.verify(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "clientId", "ex-client-id",
                                    "clientSecret", "ex-client-secret",
                                    "refreshToken", "ex-refresh-token"
                            )
                    )
            ), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/refresh",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            ), times(1));

            assertEquals("ex-response", response);
        }
    }

    @Test
    @Tag("Unit")
    public void statusRequest() throws Exception {
        // Arrange
        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class)) {
            staticMiscUtil.when(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "accessToken", "ex-access-token"
                            )
                    )
            )).thenReturn("ex-encrypted-content");

            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/status",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            )).thenReturn("ex-response");

            // Act
            final var response = RequestFactory.statusRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-access-token");

            // Assert
            staticMiscUtil.verify(() -> MiscUtil.createEncryptedContent(
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    List.of(
                            Map.of(
                                    "accessToken", "ex-access-token"
                            )
                    )
            ), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/externalAPI/status",
                    "POST",
                    new ArrayList<>(),
                    "ex-encrypted-content"
            ), times(1));

            assertEquals("ex-response", response);
        }
    }


    @Test
    @Tag("Unit")
    public void consentRequest() throws Exception {
        // Arrange
        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<URLEncoder> staticURLEncoder = mockStatic(URLEncoder.class)) {
            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/rest/user/giveConsent2?clientId=ex-encoded-client-id&scope=ex-encoded-scope&axaId=ex-encoded-foreign-id",
                    "GET",
                    List.of(Map.of("PISA_ID", "ex-pnr")),
                    null
            )).thenReturn("ex-response");

            staticURLEncoder.when(() -> URLEncoder.encode("ex-client-id", StandardCharsets.UTF_8)).thenReturn("ex-encoded-client-id");
            staticURLEncoder.when(() -> URLEncoder.encode("ex-scope", StandardCharsets.UTF_8)).thenReturn("ex-encoded-scope");
            staticURLEncoder.when(() -> URLEncoder.encode("ex-foreign-id", StandardCharsets.UTF_8)).thenReturn("ex-encoded-foreign-id");

            // Act
            final var response = RequestFactory.consentRequest("ex-base-url",
                    "ex-client-id",
                    "ex-scope",
                    "ex-foreign-id",
                    "ex-pnr");

            // Assert
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-client-id", StandardCharsets.UTF_8), times(1));
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-scope", StandardCharsets.UTF_8), times(1));
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-foreign-id", StandardCharsets.UTF_8), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/rest/user/giveConsent2?clientId=ex-encoded-client-id&scope=ex-encoded-scope&axaId=ex-encoded-foreign-id",
                    "GET",
                    List.of(Map.of("PISA_ID", "ex-pnr")),
                    null
            ), times(1));

            assertEquals("ex-response", response);
        }
    }

    @Test
    @Tag("Unit")
    public void revokeRequest() throws Exception {
        // Arrange
        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<URLEncoder> staticURLEncoder = mockStatic(URLEncoder.class)) {
            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/rest/user/revokeConsent2?clientId=ex-encoded-client-id&scope=ex-encoded-scope&axaId=ex-encoded-foreign-id",
                    "GET",
                    List.of(Map.of("PISA_ID", "ex-pnr")),
                    null
            )).thenReturn("ex-response");

            staticURLEncoder.when(() -> URLEncoder.encode("ex-client-id", StandardCharsets.UTF_8)).thenReturn("ex-encoded-client-id");
            staticURLEncoder.when(() -> URLEncoder.encode("ex-scope", StandardCharsets.UTF_8)).thenReturn("ex-encoded-scope");
            staticURLEncoder.when(() -> URLEncoder.encode("ex-foreign-id", StandardCharsets.UTF_8)).thenReturn("ex-encoded-foreign-id");

            // Act
            final var response = RequestFactory.revokeRequest("ex-base-url",
                    "ex-client-id",
                    "ex-scope",
                    "ex-foreign-id",
                    "ex-pnr");

            // Assert
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-client-id", StandardCharsets.UTF_8), times(1));
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-scope", StandardCharsets.UTF_8), times(1));
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-foreign-id", StandardCharsets.UTF_8), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/rest/user/revokeConsent2?clientId=ex-encoded-client-id&scope=ex-encoded-scope&axaId=ex-encoded-foreign-id",
                    "GET",
                    List.of(Map.of("PISA_ID", "ex-pnr")),
                    null
            ), times(1));

            assertEquals("ex-response", response);
        }
    }

    @Test
    @Tag("Unit")
    public void serviceRequest() throws Exception {
        // Arrange
        try (MockedStatic<HttpUtil> staticHttpUtil = mockStatic(HttpUtil.class);
             MockedStatic<URLEncoder> staticURLEncoder = mockStatic(URLEncoder.class)) {
            staticHttpUtil.when(() -> HttpUtil.request(
                    "ex-base-url/rest/service/ex-encoded-client-id/",
                    "GET",
                    List.of(),
                    null
            )).thenReturn("ex-response");

            staticURLEncoder.when(() -> URLEncoder.encode("ex-client-id", StandardCharsets.UTF_8)).thenReturn("ex-encoded-client-id");

            // Act
            final var response = RequestFactory.serviceRequest("ex-base-url", "ex-client-id");

            // Assert
            staticURLEncoder.verify(() -> URLEncoder.encode("ex-client-id", StandardCharsets.UTF_8), times(1));

            staticHttpUtil.verify(() -> HttpUtil.request(
                    "ex-base-url/rest/service/ex-encoded-client-id/",
                    "GET",
                    List.of(),
                    null
            ), times(1));

            assertEquals("ex-response", response);
        }
    }
}
