package com.arbetsformedlingen;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.nimbusds.jwt.JWTClaimsSet;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import picocli.CommandLine;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MainCommandTest {
    @Test
    @Tag("Unit")
    public void testMain() {
        try (MockedConstruction<CommandLine> constructionCommandLine = mockConstruction(CommandLine.class, (mock, context) -> {
            when(mock.execute(any())).thenReturn(123);
        });
             MockedConstruction<MainCommand> constructionMainCommand = mockConstruction(MainCommand.class)) {
            final var mockArgs = new String[] {"1", "2", "3"};

            MainCommand.main(mockArgs);

            assertEquals(1, constructionCommandLine.constructed().size());

            final var mockCommandLine = constructionCommandLine.constructed().get(0);

            assertEquals(1, constructionMainCommand.constructed().size());

            final var mockMainCommand = constructionMainCommand.constructed().get(0);

            verify(mockCommandLine, times(1)).execute(mockArgs);
        }
    }

    @Test
    @Tag("Integration")
    public void test_exitCode0() {
        // Arrange
        final MainCommand mainCommand = new MainCommand();
        final CommandLine cmd = new CommandLine(mainCommand);

        final StringWriter sw = new StringWriter();
        cmd.setOut(new PrintWriter(sw));

        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<KeyUtil> staticKeyUtil = mockStatic(KeyUtil.class);
             MockedStatic<RequestFactory> staticRequestFactory = mockStatic(RequestFactory.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class);
             MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class);
             MockedStatic<ValidationUtil> staticValidationUtil = mockStatic(ValidationUtil.class)) {

            staticKeyUtil.when(() -> KeyUtil.getPrivateKey(any(File.class))).thenReturn(mockRSAPrivateKey);
            staticKeyUtil.when(() -> KeyUtil.getPublicKey(any(File.class))).thenReturn(mockRSAPublicKey);

            staticRequestFactory.when(() -> RequestFactory.initRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-foreign-id",
                    "ex-pnr")).thenReturn("ex-init-response");

            final var mockInitResponseClaims = mock(JWTClaimsSet.class);
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-init-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockInitResponseClaims);

            final var mockServiceResponse = "{\"ex\": \"service-response\"}";
            staticRequestFactory.when(() -> RequestFactory.serviceRequest("ex-base-url", "ex-client-id")).thenReturn(mockServiceResponse);

            final var mockConsentResponse = "{\"ex\": \"consent-response\"}";
            staticJsonPath.when(() -> JsonPath.read(mockConsentResponse, "$.authCode")).thenReturn("ex-auth-code");
            staticRequestFactory.when(() -> RequestFactory.consentRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreign-id", "ex-pnr")).thenReturn(mockConsentResponse);

            final var mockConsentDocumentContext = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(mockConsentResponse)).thenReturn(mockConsentDocumentContext);

            when(mockConsentDocumentContext.read("$.clientId")).thenReturn("ex-client-id");
            when(mockConsentDocumentContext.read("$.name")).thenReturn("ex-client-name");
            // when(mockConsentDocumentContext.read("$.enabled")).thenReturn("ex-enabled");
            when(mockConsentDocumentContext.read("$.authCode")).thenReturn("ex-auth-code");

            staticRequestFactory.when(() -> RequestFactory.tokenRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-auth-code")).thenReturn("ex-token-response");

            final var mockTokenResponseClaims = mock(JWTClaimsSet.class);
            when(mockTokenResponseClaims.getClaim("refreshToken")).thenReturn("ex-refresh-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-token-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockTokenResponseClaims);

            staticRequestFactory.when(() -> RequestFactory.refreshRequest("ex-base-url",
                            "ex-client-name",
                            mockRSAPrivateKey,
                            mockRSAPublicKey,
                            "ex-client-id",
                            "ex-client-secret",
                            "ex-refresh-token"))
                    .thenReturn("ex-refresh-response")
                    .thenReturn("ex-refresh-response2");

            final var mockRefreshResponseClaims = mock(JWTClaimsSet.class);
            when(mockRefreshResponseClaims.getClaim("accessToken")).thenReturn("ex-access-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockRefreshResponseClaims);

            staticRequestFactory.when(() -> RequestFactory.statusRequest("ex-base-url",
                            "ex-client-name",
                            mockRSAPrivateKey,
                            mockRSAPublicKey,
                            "ex-access-token"))
                    .thenReturn("ex-status-response")
                    .thenThrow(new Exception("Response 401"));

            final var mockStatusResponseClaims = mock(JWTClaimsSet.class);
            final var mockResultContentDocumentContext = mock(DocumentContext.class);
            when(mockStatusResponseClaims.getClaim("resultContent")).thenReturn("{\"ex\": \"result-content\"}");
            staticJsonPath.when(() -> JsonPath.parse("{\"ex\": \"result-content\"}")).thenReturn(mockResultContentDocumentContext);
            when(mockResultContentDocumentContext.read("$.status")).thenReturn("Unemployed");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-status-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockStatusResponseClaims);

            final var mockRefreshResponseClaims2 = mock(JWTClaimsSet.class);
            when(mockRefreshResponseClaims2.getClaim("accessToken")).thenReturn("ex-access-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response2", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockRefreshResponseClaims2);

            final var mockRevokeResponse = "{\"ex\": \"revoke-response\"}";
            staticRequestFactory.when(() -> RequestFactory.revokeRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreign-id", "ex-pnr")).thenReturn(mockRevokeResponse);

            final var mockRevokeDocumentContext = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(mockRevokeResponse)).thenReturn(mockRevokeDocumentContext);

            when(mockRevokeDocumentContext.read("$.clientId")).thenReturn("ex-client-id");
            when(mockRevokeDocumentContext.read("$.name")).thenReturn("ex-client-name");
            // when(mockRevokeDocumentContext.read("$.enabled")).thenReturn("ex-enabled");
            when(mockRevokeDocumentContext.read("$.authCode")).thenReturn(null);

            // Act
            int exitCode = cmd.execute("--baseUrl", "ex-base-url",
                    "--scope", "ex-scope",
                    "--pnr", "ex-pnr",
                    "--foreignId", "ex-foreign-id",
                    "--clientId", "ex-client-id",
                    "--clientName", "ex-client-name",
                    "--clientSecret", "ex-client-secret",
                    "--clientPrivateKeyPath", "ex-client-private-key-path",
                    "--clientPublicKeyPath", "ex-client-public-key-path");

            // Assert
            assertEquals(0, exitCode);

            staticRequestFactory.verify(() -> RequestFactory.initRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-foreign-id",
                    "ex-pnr"), times(1));

            staticMiscUtil.verify(() -> MiscUtil.getJWSClaimsSet("ex-init-response", mockRSAPrivateKey, mockRSAPublicKey), times(1));

            staticRequestFactory.verify(() -> RequestFactory.serviceRequest("ex-base-url", "ex-client-id"), times(1));

            staticValidationUtil.verify(() -> ValidationUtil.validateServiceResponse(mockServiceResponse, "ex-client-id", "ex-client-name"), times(1));

            //staticRequestFactory.verify(() -> RequestFactory.consentRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreignId", "ex-pnr"), times(1));

            staticValidationUtil.verify(() -> ValidationUtil.validateConsentResponse(mockConsentResponse, "ex-client-id", "ex-client-name"), times(1));

            staticRequestFactory.verify(() -> RequestFactory.tokenRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-auth-code"), times(1));

            staticMiscUtil.verify(() -> MiscUtil.getJWSClaimsSet("ex-token-response", mockRSAPrivateKey, mockRSAPublicKey), times(1));

            staticRequestFactory.verify(() -> RequestFactory.refreshRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-refresh-token"), times(2));

            staticMiscUtil.verify(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response", mockRSAPrivateKey, mockRSAPublicKey), times(1));

            staticRequestFactory.verify(() -> RequestFactory.statusRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-access-token"), times(2));

            staticMiscUtil.verify(() -> MiscUtil.getJWSClaimsSet("ex-status-response", mockRSAPrivateKey, mockRSAPublicKey), times(1));

            staticMiscUtil.verify(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response2", mockRSAPrivateKey, mockRSAPublicKey), times(1));

            // staticRequestFactory.verify(() -> RequestFactory.revokeRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreignId", "ex-pnr"), times(1));

            staticValidationUtil.verify(() -> ValidationUtil.validateRevokeResponse(mockRevokeResponse, "ex-client-id", "ex-client-name"), times(1));
        }
    }

    @Test
    @Tag("Integration")
    public void test_whenUnexpectedErrorIsThrown_exitCode1() {
        // Arrange
        final MainCommand mainCommand = new MainCommand();
        final CommandLine cmd = new CommandLine(mainCommand);

        final StringWriter sw = new StringWriter();
        cmd.setOut(new PrintWriter(sw));

        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<KeyUtil> staticKeyUtil = mockStatic(KeyUtil.class);
             MockedStatic<RequestFactory> staticRequestFactory = mockStatic(RequestFactory.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class);
             MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class);
             MockedStatic<ValidationUtil> staticValidationUtil = mockStatic(ValidationUtil.class)) {

            staticKeyUtil.when(() -> KeyUtil.getPrivateKey(any(File.class))).thenReturn(mockRSAPrivateKey);
            staticKeyUtil.when(() -> KeyUtil.getPublicKey(any(File.class))).thenReturn(mockRSAPublicKey);

            staticRequestFactory.when(() -> RequestFactory.initRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-foreign-id",
                    "ex-pnr")).thenReturn("ex-init-response");

            final var mockInitResponseClaims = mock(JWTClaimsSet.class);
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-init-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockInitResponseClaims);

            final var mockServiceResponse = "{\"ex\": \"service-response\"}";
            staticRequestFactory.when(() -> RequestFactory.serviceRequest("ex-base-url", "ex-client-id")).thenReturn(mockServiceResponse);

            final var mockConsentResponse = "{\"ex\": \"consent-response\"}";
            staticJsonPath.when(() -> JsonPath.read(mockConsentResponse, "$.authCode")).thenReturn("ex-auth-code");
            staticRequestFactory.when(() -> RequestFactory.consentRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreign-id", "ex-pnr")).thenReturn(mockConsentResponse);

            final var mockConsentDocumentContext = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(mockConsentResponse)).thenReturn(mockConsentDocumentContext);

            staticRequestFactory.when(() -> RequestFactory.tokenRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-auth-code")).thenReturn("ex-token-response");

            final var mockTokenResponseClaims = mock(JWTClaimsSet.class);
            when(mockTokenResponseClaims.getClaim("refreshToken")).thenReturn("ex-refresh-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-token-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockTokenResponseClaims);

            staticRequestFactory.when(() -> RequestFactory.refreshRequest("ex-base-url",
                            "ex-client-name",
                            mockRSAPrivateKey,
                            mockRSAPublicKey,
                            "ex-client-id",
                            "ex-client-secret",
                            "ex-refresh-token"))
                    .thenReturn("ex-refresh-response")
                    .thenReturn("ex-refresh-response2");

            final var mockRefreshResponseClaims = mock(JWTClaimsSet.class);
            when(mockRefreshResponseClaims.getClaim("accessToken")).thenReturn("ex-access-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockRefreshResponseClaims);

            staticRequestFactory.when(() -> RequestFactory.statusRequest("ex-base-url",
                            "ex-client-name",
                            mockRSAPrivateKey,
                            mockRSAPublicKey,
                            "ex-access-token"))
                    .thenReturn("ex-status-response")
                    .thenThrow(new Exception("Unexpected error"));

            final var mockStatusResponseClaims = mock(JWTClaimsSet.class);
            final var mockResultContentDocumentContext = mock(DocumentContext.class);
            when(mockStatusResponseClaims.getClaim("resultContent")).thenReturn("{\"ex\": \"result-content\"}");
            staticJsonPath.when(() -> JsonPath.parse("{\"ex\": \"result-content\"}")).thenReturn(mockResultContentDocumentContext);
            when(mockResultContentDocumentContext.read("$.status")).thenReturn("Unemployed");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-status-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockStatusResponseClaims);

            final var mockRefreshResponseClaims2 = mock(JWTClaimsSet.class);
            when(mockRefreshResponseClaims2.getClaim("accessToken")).thenReturn("ex-access-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response2", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockRefreshResponseClaims2);

            final var mockRevokeResponse = "{\"ex\": \"revoke-response\"}";
            staticRequestFactory.when(() -> RequestFactory.revokeRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreign-id", "ex-pnr")).thenReturn(mockRevokeResponse);

            final var mockRevokeDocumentContext = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(mockRevokeResponse)).thenReturn(mockRevokeDocumentContext);

            when(mockRevokeDocumentContext.read("$.clientId")).thenReturn("ex-client-id");
            when(mockRevokeDocumentContext.read("$.name")).thenReturn("ex-client-name");
            // when(mockRevokeDocumentContext.read("$.enabled")).thenReturn("ex-enabled");
            when(mockRevokeDocumentContext.read("$.authCode")).thenReturn(null);

            // Act
            int exitCode = cmd.execute("--baseUrl", "ex-base-url",
                    "--scope", "ex-scope",
                    "--pnr", "ex-pnr",
                    "--foreignId", "ex-foreign-id",
                    "--clientId", "ex-client-id",
                    "--clientName", "ex-client-name",
                    "--clientSecret", "ex-client-secret",
                    "--clientPrivateKeyPath", "ex-client-private-key-path",
                    "--clientPublicKeyPath", "ex-client-public-key-path");

            // Assert
            assertEquals(1, exitCode);
        }
    }

    @Test
    @Tag("Integration")
    public void test_whenTheFinalStatusRequestDoesNotThrow_exitCode1() {
        // Arrange
        final MainCommand mainCommand = new MainCommand();
        final CommandLine cmd = new CommandLine(mainCommand);

        final StringWriter sw = new StringWriter();
        cmd.setOut(new PrintWriter(sw));

        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        try (MockedStatic<KeyUtil> staticKeyUtil = mockStatic(KeyUtil.class);
             MockedStatic<RequestFactory> staticRequestFactory = mockStatic(RequestFactory.class);
             MockedStatic<MiscUtil> staticMiscUtil = mockStatic(MiscUtil.class);
             MockedStatic<JsonPath> staticJsonPath = mockStatic(JsonPath.class);
             MockedStatic<ValidationUtil> staticValidationUtil = mockStatic(ValidationUtil.class)) {

            staticKeyUtil.when(() -> KeyUtil.getPrivateKey(any(File.class))).thenReturn(mockRSAPrivateKey);
            staticKeyUtil.when(() -> KeyUtil.getPublicKey(any(File.class))).thenReturn(mockRSAPublicKey);

            staticRequestFactory.when(() -> RequestFactory.initRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-foreign-id",
                    "ex-pnr")).thenReturn("ex-init-response");

            final var mockInitResponseClaims = mock(JWTClaimsSet.class);
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-init-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockInitResponseClaims);

            final var mockServiceResponse = "{\"ex\": \"service-response\"}";
            staticRequestFactory.when(() -> RequestFactory.serviceRequest("ex-base-url", "ex-client-id")).thenReturn(mockServiceResponse);

            final var mockConsentResponse = "{\"ex\": \"consent-response\"}";
            staticJsonPath.when(() -> JsonPath.read(mockConsentResponse, "$.authCode")).thenReturn("ex-auth-code");
            staticRequestFactory.when(() -> RequestFactory.consentRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreign-id", "ex-pnr")).thenReturn(mockConsentResponse);

            final var mockConsentDocumentContext = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(mockConsentResponse)).thenReturn(mockConsentDocumentContext);

            staticRequestFactory.when(() -> RequestFactory.tokenRequest("ex-base-url",
                    "ex-client-name",
                    mockRSAPrivateKey,
                    mockRSAPublicKey,
                    "ex-client-id",
                    "ex-client-secret",
                    "ex-auth-code")).thenReturn("ex-token-response");

            final var mockTokenResponseClaims = mock(JWTClaimsSet.class);
            when(mockTokenResponseClaims.getClaim("refreshToken")).thenReturn("ex-refresh-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-token-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockTokenResponseClaims);

            staticRequestFactory.when(() -> RequestFactory.refreshRequest("ex-base-url",
                            "ex-client-name",
                            mockRSAPrivateKey,
                            mockRSAPublicKey,
                            "ex-client-id",
                            "ex-client-secret",
                            "ex-refresh-token"))
                    .thenReturn("ex-refresh-response")
                    .thenReturn("ex-refresh-response2");

            final var mockRefreshResponseClaims = mock(JWTClaimsSet.class);
            when(mockRefreshResponseClaims.getClaim("accessToken")).thenReturn("ex-access-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockRefreshResponseClaims);

            staticRequestFactory.when(() -> RequestFactory.statusRequest("ex-base-url",
                            "ex-client-name",
                            mockRSAPrivateKey,
                            mockRSAPublicKey,
                            "ex-access-token"))
                    .thenReturn("ex-status-response")
                    .thenReturn("ex-status-response2");

            final var mockStatusResponseClaims = mock(JWTClaimsSet.class);
            final var mockResultContentDocumentContext = mock(DocumentContext.class);
            when(mockStatusResponseClaims.getClaim("resultContent")).thenReturn("{\"ex\": \"result-content\"}");
            staticJsonPath.when(() -> JsonPath.parse("{\"ex\": \"result-content\"}")).thenReturn(mockResultContentDocumentContext);
            when(mockResultContentDocumentContext.read("$.status")).thenReturn("Unemployed");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-status-response", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockStatusResponseClaims);

            final var mockRefreshResponseClaims2 = mock(JWTClaimsSet.class);
            when(mockRefreshResponseClaims2.getClaim("accessToken")).thenReturn("ex-access-token");
            staticMiscUtil.when(() -> MiscUtil.getJWSClaimsSet("ex-refresh-response2", mockRSAPrivateKey, mockRSAPublicKey)).thenReturn(mockRefreshResponseClaims2);

            final var mockRevokeResponse = "{\"ex\": \"revoke-response\"}";
            staticRequestFactory.when(() -> RequestFactory.revokeRequest("ex-base-url", "ex-client-id", "ex-scope", "ex-foreign-id", "ex-pnr")).thenReturn(mockRevokeResponse);

            final var mockRevokeDocumentContext = mock(DocumentContext.class);
            staticJsonPath.when(() -> JsonPath.parse(mockRevokeResponse)).thenReturn(mockRevokeDocumentContext);

            when(mockRevokeDocumentContext.read("$.clientId")).thenReturn("ex-client-id");
            when(mockRevokeDocumentContext.read("$.name")).thenReturn("ex-client-name");
            // when(mockRevokeDocumentContext.read("$.enabled")).thenReturn("ex-enabled");
            when(mockRevokeDocumentContext.read("$.authCode")).thenReturn(null);

            // Act
            int exitCode = cmd.execute("--baseUrl", "ex-base-url",
                    "--scope", "ex-scope",
                    "--pnr", "ex-pnr",
                    "--foreignId", "ex-foreign-id",
                    "--clientId", "ex-client-id",
                    "--clientName", "ex-client-name",
                    "--clientSecret", "ex-client-secret",
                    "--clientPrivateKeyPath", "ex-client-private-key-path",
                    "--clientPublicKeyPath", "ex-client-public-key-path");

            assertEquals(1, exitCode);
        }
    }
}
