package com.arbetsformedlingen;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class HttpUtilTest {
    @Test
    @Tag("Unit")
    public void construct() {
        new HttpUtil();
    }

    @Test
    @Tag("Unit")
    public void request() throws Exception {
        // Arrange
        final var mockURL = mock(URL.class);

        final var mockInputStream = mock(InputStream.class);

        final var mockHttpURLConnection = mock(HttpURLConnection.class);
        when(mockURL.openConnection()).thenReturn(mockHttpURLConnection);
        when(mockHttpURLConnection.getInputStream()).thenReturn(mockInputStream);
        when(mockHttpURLConnection.getResponseCode()).thenReturn(200);

        final var mockOutputStream = mock(OutputStream.class);
        when(mockHttpURLConnection.getOutputStream()).thenReturn(mockOutputStream);

        final var mockOutputStreamWriter = mock(OutputStreamWriter.class);

        final var mockBufferedInputStream = mock(BufferedInputStream.class);

        when(mockBufferedInputStream.read())
                .thenReturn(1)
                .thenReturn(-1);

        final var mockByteArrayOutputStream = mock(ByteArrayOutputStream.class);

        when(mockByteArrayOutputStream.toString()).thenReturn("ex-body");

        try (MockedStatic<UrlFactory> staticUrlFactory = mockStatic(UrlFactory.class);
             MockedStatic<StreamFactory> staticStreamFactory = mockStatic(StreamFactory.class)) {
            staticUrlFactory.when(() -> UrlFactory.createURL("ex-path")).thenReturn(mockURL);
            staticStreamFactory.when(() -> StreamFactory.createOutputStreamWriter(mockOutputStream)).thenReturn(mockOutputStreamWriter);
            staticStreamFactory.when(() -> StreamFactory.createBufferedInputStream(mockHttpURLConnection.getInputStream())).thenReturn(mockBufferedInputStream);
            staticStreamFactory.when(StreamFactory::createByteArrayOutputStream).thenReturn(mockByteArrayOutputStream);

            // Act
            final var body = HttpUtil.request("ex-path", "ex-method",
                    List.of(Map.of("ex-header-key", "ex-header-value")) ,
                    "ex-encrypted-content");

            // Assert
            staticUrlFactory.verify(() -> UrlFactory.createURL("ex-path"), times(1));
            verify(mockURL, times(1)).openConnection();
            verify(mockHttpURLConnection, times(1)).setRequestMethod("ex-method");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("Content-Type", "application/json");
            verify(mockHttpURLConnection, times(1)).getRequestProperty("Content-Type");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("User-Agent", "testatron-cli");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("ex-header-key", "ex-header-value");
            verify(mockHttpURLConnection, times(1)).getRequestProperty("ex-header-key");
            verify(mockHttpURLConnection, times(1)).setDoOutput(true);
            verify(mockHttpURLConnection, times(1)).getOutputStream();
            staticStreamFactory.verify(() -> StreamFactory.createOutputStreamWriter(mockOutputStream), times(1));
            verify(mockOutputStreamWriter, times(1)).write("{\"encryptedContent\":\"ex-encrypted-content\"}");
            verify(mockOutputStreamWriter, times(1)).flush();
            verify(mockOutputStreamWriter, times(1)).close();
            verify(mockOutputStream, times(1)).close();
            verify(mockHttpURLConnection, times(1)).connect();
            verify(mockHttpURLConnection, times(1)).getResponseCode();
            staticStreamFactory.verify(() -> StreamFactory.createBufferedInputStream(mockInputStream), times(1));
            staticStreamFactory.verify(() -> StreamFactory.createByteArrayOutputStream(), times(1));
            verify(mockBufferedInputStream, times(2)).read();
            verify(mockByteArrayOutputStream, times(1)).write(1);
            verify(mockHttpURLConnection, times(1)).disconnect();

            assertEquals("ex-body", body);
        }
    }

    @Test
    @Tag("Unit")
    public void request_whenEncryptedContentIsNull() throws Exception {
        // Arrange
        final var mockURL = mock(URL.class);

        final var mockInputStream = mock(InputStream.class);

        final var mockHttpURLConnection = mock(HttpURLConnection.class);
        when(mockURL.openConnection()).thenReturn(mockHttpURLConnection);
        when(mockHttpURLConnection.getInputStream()).thenReturn(mockInputStream);
        when(mockHttpURLConnection.getResponseCode()).thenReturn(200);

        final var mockOutputStream = mock(OutputStream.class);
        when(mockHttpURLConnection.getOutputStream()).thenReturn(mockOutputStream);

        final var mockOutputStreamWriter = mock(OutputStreamWriter.class);

        final var mockBufferedInputStream = mock(BufferedInputStream.class);

        when(mockBufferedInputStream.read())
                .thenReturn(1)
                .thenReturn(-1);

        final var mockByteArrayOutputStream = mock(ByteArrayOutputStream.class);

        when(mockByteArrayOutputStream.toString()).thenReturn("ex-body");

        try (MockedStatic<UrlFactory> staticUrlFactory = mockStatic(UrlFactory.class);
             MockedStatic<StreamFactory> staticStreamFactory = mockStatic(StreamFactory.class)) {
            staticUrlFactory.when(() -> UrlFactory.createURL("ex-path")).thenReturn(mockURL);
            staticStreamFactory.when(() -> StreamFactory.createOutputStreamWriter(mockOutputStream)).thenReturn(mockOutputStreamWriter);
            staticStreamFactory.when(() -> StreamFactory.createBufferedInputStream(mockHttpURLConnection.getInputStream())).thenReturn(mockBufferedInputStream);
            staticStreamFactory.when(StreamFactory::createByteArrayOutputStream).thenReturn(mockByteArrayOutputStream);

            // Act
            final var body = HttpUtil.request("ex-path", "ex-method",
                    List.of(Map.of("ex-header-key", "ex-header-value")) ,
                    null);

            // Assert
            staticUrlFactory.verify(() -> UrlFactory.createURL("ex-path"), times(1));
            verify(mockURL, times(1)).openConnection();
            verify(mockHttpURLConnection, times(1)).setRequestMethod("ex-method");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("Content-Type", "application/json");
            verify(mockHttpURLConnection, times(1)).getRequestProperty("Content-Type");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("User-Agent", "testatron-cli");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("ex-header-key", "ex-header-value");
            verify(mockHttpURLConnection, times(1)).getRequestProperty("ex-header-key");
            verify(mockHttpURLConnection, times(0)).setDoOutput(true);
            verify(mockHttpURLConnection, times(0)).getOutputStream();
            staticStreamFactory.verify(() -> StreamFactory.createOutputStreamWriter(mockOutputStream), times(0));
            verify(mockOutputStreamWriter, times(0)).write("{\"encryptedContent\":\"ex-encrypted-content\"}");
            verify(mockOutputStreamWriter, times(0)).flush();
            verify(mockOutputStreamWriter, times(0)).close();
            verify(mockOutputStream, times(0)).close();
            verify(mockHttpURLConnection, times(1)).connect();
            verify(mockHttpURLConnection, times(1)).getResponseCode();
            staticStreamFactory.verify(() -> StreamFactory.createBufferedInputStream(mockInputStream), times(1));
            staticStreamFactory.verify(() -> StreamFactory.createByteArrayOutputStream(), times(1));
            verify(mockBufferedInputStream, times(2)).read();
            verify(mockByteArrayOutputStream, times(1)).write(1);
            verify(mockHttpURLConnection, times(1)).disconnect();

            assertEquals("ex-body", body);
        }
    }

    @Test
    @Tag("Unit")
    public void request_whenResponseStatusIs401_throws() throws Exception {
        // Arrange
        final var mockURL = mock(URL.class);

        final var mockInputStream = mock(InputStream.class);

        final var mockHttpURLConnection = mock(HttpURLConnection.class);
        when(mockURL.openConnection()).thenReturn(mockHttpURLConnection);
        when(mockHttpURLConnection.getInputStream()).thenReturn(mockInputStream);
        when(mockHttpURLConnection.getResponseCode()).thenReturn(401);

        final var mockOutputStream = mock(OutputStream.class);
        when(mockHttpURLConnection.getOutputStream()).thenReturn(mockOutputStream);

        final var mockOutputStreamWriter = mock(OutputStreamWriter.class);

        final var mockBufferedInputStream = mock(BufferedInputStream.class);

        when(mockBufferedInputStream.read())
                .thenReturn(1)
                .thenReturn(-1);

        final var mockByteArrayOutputStream = mock(ByteArrayOutputStream.class);

        when(mockByteArrayOutputStream.toString()).thenReturn("ex-body");

        try (MockedStatic<UrlFactory> staticUrlFactory = mockStatic(UrlFactory.class);
             MockedStatic<StreamFactory> staticStreamFactory = mockStatic(StreamFactory.class)) {
            staticUrlFactory.when(() -> UrlFactory.createURL("ex-path")).thenReturn(mockURL);
            staticStreamFactory.when(() -> StreamFactory.createOutputStreamWriter(mockOutputStream)).thenReturn(mockOutputStreamWriter);
            staticStreamFactory.when(() -> StreamFactory.createBufferedInputStream(mockHttpURLConnection.getInputStream())).thenReturn(mockBufferedInputStream);
            staticStreamFactory.when(StreamFactory::createByteArrayOutputStream).thenReturn(mockByteArrayOutputStream);

            // Act
            assertThrows(Exception.class, () -> HttpUtil.request("ex-path", "ex-method",
                    List.of(Map.of("ex-header-key", "ex-header-value")) ,
                    "ex-encrypted-content"));

            // Assert
            staticUrlFactory.verify(() -> UrlFactory.createURL("ex-path"), times(1));
            verify(mockURL, times(1)).openConnection();
            verify(mockHttpURLConnection, times(1)).setRequestMethod("ex-method");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("Content-Type", "application/json");
            verify(mockHttpURLConnection, times(1)).getRequestProperty("Content-Type");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("User-Agent", "testatron-cli");
            verify(mockHttpURLConnection, times(1)).setRequestProperty("ex-header-key", "ex-header-value");
            verify(mockHttpURLConnection, times(1)).getRequestProperty("ex-header-key");
            verify(mockHttpURLConnection, times(1)).setDoOutput(true);
            verify(mockHttpURLConnection, times(1)).getOutputStream();
            staticStreamFactory.verify(() -> StreamFactory.createOutputStreamWriter(mockOutputStream), times(1));
            verify(mockOutputStreamWriter, times(1)).write("{\"encryptedContent\":\"ex-encrypted-content\"}");
            verify(mockOutputStreamWriter, times(1)).flush();
            verify(mockOutputStreamWriter, times(1)).close();
            verify(mockOutputStream, times(1)).close();
            verify(mockHttpURLConnection, times(1)).connect();
            verify(mockHttpURLConnection, times(1)).getResponseCode();
            staticStreamFactory.verify(() -> StreamFactory.createBufferedInputStream(mockInputStream), times(0));
            staticStreamFactory.verify(() -> StreamFactory.createByteArrayOutputStream(), times(0));
            verify(mockBufferedInputStream, times(0)).read();
            verify(mockByteArrayOutputStream, times(0)).write(1);
            verify(mockHttpURLConnection, times(1)).disconnect();
        }
    }
}
