package com.arbetsformedlingen;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class KeyUtilTest {
    @Test
    @Tag("Unit")
    public void construct() {
        new KeyUtil();
    }

    @Test
    @Tag("Unit")
    public void getPrivateKey() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        final var mockLogger = mock(Logger.class);
        final var mockKeyFactory = mock(KeyFactory.class);
        final var mockFile = mock(File.class);
        final var mockPath = mock(Path.class);
        final var mockBytes = new byte[] {};
        final var mockRSAPrivateKey = mock(RSAPrivateKey.class);

        when(mockFile.toPath()).thenReturn(mockPath);

        final var constructionPKCS8EncodedKeySpecArgs = new HashMap<PKCS8EncodedKeySpec, List<?>>();

        try (MockedStatic<LoggerFactory> staticLoggerFactory = mockStatic(LoggerFactory.class);
             MockedStatic<KeyFactory> staticKeyFactory = mockStatic(KeyFactory.class);
             MockedStatic<Files> staticFiles = mockStatic(Files.class);
             MockedConstruction<PKCS8EncodedKeySpec> constructionPKCS8EncodedKeySpec = mockConstruction(PKCS8EncodedKeySpec.class, (mock, context) -> {
                 constructionPKCS8EncodedKeySpecArgs.put(mock, context.arguments());
             })) {
            staticLoggerFactory.when(() -> LoggerFactory.getLogger(KeyUtil.class)).thenReturn(mockLogger);
            staticKeyFactory.when(() -> KeyFactory.getInstance("RSA")).thenReturn(mockKeyFactory);
            staticFiles.when(() -> Files.readAllBytes(mockPath)).thenReturn(mockBytes);
            when(mockKeyFactory.generatePrivate(any(PKCS8EncodedKeySpec.class))).thenReturn(mockRSAPrivateKey);

            // Act
            final var privateKey = KeyUtil.getPrivateKey(mockFile);

            // Assert
            staticKeyFactory.verify(() -> KeyFactory.getInstance("RSA"), times(1));
            staticFiles.verify(() -> Files.readAllBytes(mockPath), times(1));

            verify(mockFile, times(1)).toPath();
            verify(mockPath, times(1)).toAbsolutePath();

            assertEquals(1, constructionPKCS8EncodedKeySpec.constructed().size());

            final var mockPKCS8EncodedKeySpec = constructionPKCS8EncodedKeySpec.constructed().get(0);

            verify(mockKeyFactory, times(1)).generatePrivate(mockPKCS8EncodedKeySpec);

            assertEquals(mockBytes, constructionPKCS8EncodedKeySpecArgs.get(mockPKCS8EncodedKeySpec).get(0));

            assertEquals(mockRSAPrivateKey, privateKey);
        }
    }

    @Test
    @Tag("Unit")
    public void getPublicKey() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        final var mockLogger = mock(Logger.class);
        final var mockKeyFactory = mock(KeyFactory.class);
        final var mockFile = mock(File.class);
        final var mockPath = mock(Path.class);
        final var mockBytes = new byte[] {};
        final var mockRSAPublicKey = mock(RSAPublicKey.class);

        when(mockFile.toPath()).thenReturn(mockPath);

        final var constructionX509EncodedKeySpecArgs = new HashMap<X509EncodedKeySpec, List<?>>();

        try (MockedStatic<LoggerFactory> staticLoggerFactory = mockStatic(LoggerFactory.class);
             MockedStatic<KeyFactory> staticKeyFactory = mockStatic(KeyFactory.class);
             MockedStatic<Files> staticFiles = mockStatic(Files.class);
             MockedConstruction<X509EncodedKeySpec> constructionX509EncodedKeySpec = mockConstruction(X509EncodedKeySpec.class, (mock, context) -> {
                 constructionX509EncodedKeySpecArgs.put(mock, context.arguments());
             })) {
            staticLoggerFactory.when(() -> LoggerFactory.getLogger(KeyUtil.class)).thenReturn(mockLogger);
            staticKeyFactory.when(() -> KeyFactory.getInstance("RSA")).thenReturn(mockKeyFactory);
            staticFiles.when(() -> Files.readAllBytes(mockPath)).thenReturn(mockBytes);
            when(mockKeyFactory.generatePublic(any(X509EncodedKeySpec.class))).thenReturn(mockRSAPublicKey);

            // Act
            final var publicKey = KeyUtil.getPublicKey(mockFile);

            // Assert
            staticKeyFactory.verify(() -> KeyFactory.getInstance("RSA"), times(1));
            staticFiles.verify(() -> Files.readAllBytes(mockPath), times(1));

            verify(mockFile, times(1)).toPath();
            verify(mockPath, times(1)).toAbsolutePath();

            assertEquals(1, constructionX509EncodedKeySpec.constructed().size());

            final var mockX509EncodedKeySpec = constructionX509EncodedKeySpec.constructed().get(0);

            verify(mockKeyFactory, times(1)).generatePublic(mockX509EncodedKeySpec);

            assertEquals(mockBytes, constructionX509EncodedKeySpecArgs.get(mockX509EncodedKeySpec).get(0));

            assertEquals(mockRSAPublicKey, publicKey);
        }
    }
}
