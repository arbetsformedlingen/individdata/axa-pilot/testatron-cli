package com.arbetsformedlingen;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;

public class StreamFactoryTest {
    @Test
    @Tag("Unit")
    public void construct() {
        new StreamFactory();
    }

    @Test
    @Tag("Unit")
    public void createOutputStreamWriter() {
        // Arrange
        final var mockOutputStream = mock(OutputStream.class);

        final var constructionOutputStreamWriterArgs = new HashMap<OutputStreamWriter, List<?>>();

        try (MockedConstruction<OutputStreamWriter> constructionOutputStreamWriter = mockConstruction(OutputStreamWriter.class, (mock, context) -> {
            constructionOutputStreamWriterArgs.put(mock, context.arguments());
        })) {
            // Act
            final var outputStreamWriter = StreamFactory.createOutputStreamWriter(mockOutputStream);

            // Assert
            assertEquals(1, constructionOutputStreamWriter.constructed().size());

            final var mockOutputStreamWriter = constructionOutputStreamWriter.constructed().get(0);

            assertEquals(2, constructionOutputStreamWriterArgs.get(mockOutputStreamWriter).size());
            assertEquals(mockOutputStream, constructionOutputStreamWriterArgs.get(mockOutputStreamWriter).get(0));
            assertEquals(StandardCharsets.UTF_8, constructionOutputStreamWriterArgs.get(mockOutputStreamWriter).get(1));

            assertEquals(mockOutputStreamWriter, outputStreamWriter);
        }
    }

    @Test
    @Tag("Unit")
    public void createBufferedInputStream() {
        // Arrange
        final var mockInputStream = mock(InputStream.class);

        final var constructionBufferedInputStreamArgs = new HashMap<BufferedInputStream, List<?>>();

        try (MockedConstruction<BufferedInputStream> constructionBufferedInputStream = mockConstruction(BufferedInputStream.class, (mock, context) -> {
            constructionBufferedInputStreamArgs.put(mock, context.arguments());
        })) {
            // Act
            final var bufferedInputStream = StreamFactory.createBufferedInputStream(mockInputStream);

            // Assert
            assertEquals(1, constructionBufferedInputStream.constructed().size());

            final var mockBufferedInputStream = constructionBufferedInputStream.constructed().get(0);

            assertEquals(1, constructionBufferedInputStreamArgs.get(mockBufferedInputStream).size());
            assertEquals(mockInputStream, constructionBufferedInputStreamArgs.get(mockBufferedInputStream).get(0));

            assertEquals(mockBufferedInputStream, bufferedInputStream);
        }
    }

    @Test
    @Tag("Unit")
    public void createByteArrayOutputStream() {
        // Arrange
        try (MockedConstruction<ByteArrayOutputStream> constructionByteArrayOutputStream = mockConstruction(ByteArrayOutputStream.class)) {
            // Act
            final var byteArrayOutputStream = StreamFactory.createByteArrayOutputStream();

            // Assert
            assertEquals(1, constructionByteArrayOutputStream.constructed().size());

            final var mockBufferedInputStream = constructionByteArrayOutputStream.constructed().get(0);

            assertEquals(mockBufferedInputStream, byteArrayOutputStream);
        }
    }
}
