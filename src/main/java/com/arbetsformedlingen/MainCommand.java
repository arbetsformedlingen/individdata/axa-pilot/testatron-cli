package com.arbetsformedlingen;

import com.jayway.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import static picocli.CommandLine.Option;
import static picocli.CommandLine.Command;

import java.io.File;
import java.util.concurrent.Callable;

@Command(description = "This command will simulate the behavior of client & end users towards the backend api")
public class MainCommand implements Callable<Integer> {
    private static final Logger logger = LoggerFactory.getLogger(MainCommand.class);

    @Option(names = {"--baseUrl"}, required = true)
    private String baseUrl;

    @Option(names = {"--scope"}, required = true)
    private String scope;

    @Option(names = {"--pnr"}, required = true)
    private String pnr;

    @Option(names = {"--foreignId"}, required = true)
    private String foreignId;

    @Option(names = {"--clientName"}, required = true)
    private String clientName;

    @Option(names = {"--clientId"}, required = true)
    private String clientId;

    @Option(names = {"--clientSecret"}, required = true)
    private String clientSecret;

    @Option(names = {"--clientPrivateKeyPath"}, required = true)
    private String clientPrivateKeyPath;

    @Option(names = {"--clientPublicKeyPath"}, required = true)
    private String clientPublicKeyPath;

    public static void main(String[] args) {
        new CommandLine(new MainCommand()).execute(args);
    }

    @Override
    public Integer call() {
        try {
            final var clientPrivateKey = KeyUtil.getPrivateKey(new File(clientPrivateKeyPath));
            final var afPublicKey = KeyUtil.getPublicKey(new File(clientPublicKeyPath));

            // Init
            final var initResponse = RequestFactory.initRequest(baseUrl, clientName, clientPrivateKey, afPublicKey, clientId, clientSecret, foreignId, pnr);
            final var initResponseClaims = MiscUtil.getJWSClaimsSet(initResponse, clientPrivateKey, afPublicKey);

            // Get service info
            final var serviceResponse = RequestFactory.serviceRequest(baseUrl, clientId);

            ValidationUtil.validateServiceResponse(serviceResponse, clientId, clientName);

            // Give consent
            final var consentResponse = RequestFactory.consentRequest(baseUrl, clientId, scope, foreignId, pnr);

            ValidationUtil.validateConsentResponse(consentResponse, clientId, clientName);

            // Token
            final var consentResponseAuthCode = (String) JsonPath.read(consentResponse, "$.authCode");
            final var tokenResponse = RequestFactory.tokenRequest(baseUrl, clientName, clientPrivateKey, afPublicKey, clientId, clientSecret, consentResponseAuthCode);
            final var tokenResponseClaims = MiscUtil.getJWSClaimsSet(tokenResponse, clientPrivateKey, afPublicKey);

            // Refresh
            final var tokenResponseClaimsConsent = tokenResponseClaims.getClaim("refreshToken");
            final var refreshResponse = RequestFactory.refreshRequest(baseUrl, clientName, clientPrivateKey, afPublicKey, clientId, clientSecret, tokenResponseClaimsConsent.toString());
            final var refreshResponseClaims = MiscUtil.getJWSClaimsSet(refreshResponse, clientPrivateKey, afPublicKey);

            // Status
            final var refreshResponseClaimsAccessToken = refreshResponseClaims.getClaim("accessToken");
            final var statusResponse = RequestFactory.statusRequest(baseUrl, clientName, clientPrivateKey, afPublicKey, refreshResponseClaimsAccessToken.toString());
            final var statusResponseClaims = MiscUtil.getJWSClaimsSet(statusResponse, clientPrivateKey, afPublicKey);

            final var statusResponseClaimsResultContent = statusResponseClaims.getClaim("resultContent");

            ValidationUtil.validateStatusResponse(statusResponseClaimsResultContent.toString());

            // Refresh
            final var refreshResponse2 = RequestFactory.refreshRequest(baseUrl, clientName, clientPrivateKey, afPublicKey, clientId, clientSecret, tokenResponseClaims.getClaim("refreshToken").toString());
            final var refreshResponseClaims2 = MiscUtil.getJWSClaimsSet(refreshResponse2, clientPrivateKey, afPublicKey);

            // Revoke consent
            final var revokeResponse = RequestFactory.revokeRequest(baseUrl, clientId, scope, foreignId, pnr);

            ValidationUtil.validateRevokeResponse(revokeResponse, clientId, clientName);

            // Status revoked
            try {
                RequestFactory.statusRequest(baseUrl, clientName, clientPrivateKey, afPublicKey, refreshResponseClaims.getClaim("accessToken").toString());
                throw new Exception("The final status request (after revocation) must throw, but it did not");
            } catch (Exception e) {
                if (!e.getMessage().equals("Response 401")) {
                    throw e;
                }
                else {
                    logger.info("Other error");
                }
            }

            logger.info("Integration test succeeded");
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Integration test failed");
            return 1;
        }
    }
}
