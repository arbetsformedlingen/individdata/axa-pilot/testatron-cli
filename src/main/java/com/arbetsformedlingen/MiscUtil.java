package com.arbetsformedlingen;

import com.jayway.jsonpath.JsonPath;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MiscUtil {
    private static final Logger logger = LoggerFactory.getLogger(MiscUtil.class);

    public static String createEncryptedContent(String clientName, RSAPrivateKey clientPrivateKey, final RSAPublicKey clientPublicKey, final List<Map<String, String>> claims) throws JOSEException {
        // Signed JWT
        String jwsSerialized;
        {
            logger.debug("Creating inner JWT (for signing)...");
            final String jwtID = UUID.randomUUID().toString();
            final Date notBeforeTime = Date.from(Instant.now().truncatedTo(ChronoUnit.SECONDS));
            final Date expirationTime = Date.from(Instant.now().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS));
            final JWTClaimsSet.Builder jwtBuilder = new JWTClaimsSet.Builder()
                    .jwtID(jwtID)
                    .issuer(clientName)
                    .notBeforeTime(notBeforeTime)
                    .expirationTime(expirationTime);
            logger.debug("Inner JWT claim \"jwtID\": {}", jwtID);
            logger.debug("Inner JWT claim \"issuer\": {}", clientName);
            logger.debug("Inner JWT claim \"notBeforeTime\": {}", notBeforeTime);
            logger.debug("Inner JWT claim \"expirationTime\": {}", expirationTime);

            for (var claim : claims) {
                for (var key : claim.keySet()) {
                    var value = claim.get(key);
                    jwtBuilder.claim(key, value);
                    logger.debug("Inner JWT claim \"{}\": {}", key, value);
                }
            }

            logger.debug("Signing inner JWT...");
            final JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
            final SignedJWT jws = new SignedJWT(jwsHeader, jwtBuilder.build());
            jws.sign(new RSASSASigner(clientPrivateKey));
            logger.info("Signed inner JWT");

            logger.debug("Serializing the [signed] inner JWT...");
            jwsSerialized = jws.serialize();
            logger.info("Serialized the [signed] inner JWT ({} bytes)", jwsSerialized.getBytes(StandardCharsets.UTF_8).length);
        }

        // Encrypted JWT
        String jweSerialized;
        {
            logger.info("Creating outer JWT (for encryption)...");
            final String jwtID = UUID.randomUUID().toString();
            final Date notBeforeTime = Date.from(Instant.now().truncatedTo(ChronoUnit.SECONDS));
            final Date expirationTime = Date.from(Instant.now().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS));
            final JWTClaimsSet jweClaims = new JWTClaimsSet.Builder()
                    .jwtID(jwtID)
                    .issuer(clientName)
                    .claim("payload", jwsSerialized)
                    .notBeforeTime(notBeforeTime)
                    .expirationTime(expirationTime)
                    .build();
            logger.debug("Outer JWT claim \"jwtID\": {}", jwtID);
            logger.debug("Outer JWT claim \"issuer\": {}", clientName);
            logger.debug("Outer JWT claim \"notBeforeTime\": {}", notBeforeTime);
            logger.debug("Outer JWT claim \"expirationTime\": {}", expirationTime);
            logger.debug("Outer JWT claim \"payload\": {}", jwsSerialized);

            logger.debug("Encrypting outer JWT...");
            final JWEHeader jweHeader = new JWEHeader(JWEAlgorithm.RSA_OAEP_512, EncryptionMethod.A128GCM);
            final EncryptedJWT jwe = new EncryptedJWT(jweHeader, jweClaims);
            jwe.encrypt(new RSAEncrypter(clientPublicKey));
            logger.info("Encrypted outer JWT");

            logger.debug("Serializing the [encrypted] outer JWT...");
            jweSerialized = jwe.serialize();
            logger.info("Serialized the [encrypted] outer JWT ({} bytes)", jweSerialized.getBytes(StandardCharsets.UTF_8).length);
        }

        logger.info("Created encryptedContent of the [encrypted] outer JWT (which includes the [signed] inner JWT)");
        return jweSerialized;
    }

    public static JWTClaimsSet getJWSClaimsSet(String responseBody, RSAPrivateKey privateKey, RSAPublicKey publicKey) throws ParseException, JOSEException {
        if (responseBody == null) {
            logger.debug("Empty response body, no claims to extract");
            return null;
        }

        logger.debug("Extracting claims from response body...");

        logger.debug("Parsing [encrypted] outer JWT from response body...");
        final String encryptedContent = JsonPath.read(responseBody, "$.encryptedContent");
        final EncryptedJWT jwe = EncryptedJWT.parse(encryptedContent);
        logger.debug("Parsed [encrypted] outer JWT from response body");

        logger.debug("Decrypting [encrypted] outer JWT...");
        jwe.decrypt(new RSADecrypter(privateKey));
        logger.debug("Decrypted [encrypted] outer JWT");

        logger.debug("Extracting claim \"payload\" from outer JWT...");
        final JWTClaimsSet claimsJWE = jwe.getJWTClaimsSet();
        final String encodedJWS = (String) claimsJWE.getClaim("payload");
        logger.debug("Extracted claim \"payload\" from outer JWT: {}", encodedJWS);

        logger.debug("Parsing [signed] inner JWT from outer JWT (claim \"payload\")...");
        final SignedJWT jws = SignedJWT.parse(encodedJWS);
        logger.debug("Parsed [signed] inner JWT from outer JWT (claim \"payload\")");

        logger.debug("Verifying signature of [signed] inner JWT...");
        if (!jws.verify(new RSASSAVerifier(publicKey))) {
            logger.error("Signature verification failed of [signed] inner JWT");
            throw new Error("Signature verification failed");
        }
        logger.debug("Verified signature of [signed] inner JWT");

        logger.debug("Extracting all claims from [signed] inner JWT...");
        final JWTClaimsSet claimsJWS = jws.getJWTClaimsSet();

        final Map<String, Object> jwsClaims = claimsJWS.getClaims();
        for (var key : jwsClaims.keySet()) {
            var value = jwsClaims.get(key);
            logger.debug("Extracted claim from [signed] inner JWT \"{}\": {}", key, value);
        }
        logger.debug("Extracted all claims from [signed] inner JWT");

        return claimsJWS;
    }
}
