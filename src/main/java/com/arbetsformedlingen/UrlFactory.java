package com.arbetsformedlingen;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlFactory {
    public static URL createURL(String url) throws MalformedURLException {
        return new URL(url);
    }
}
