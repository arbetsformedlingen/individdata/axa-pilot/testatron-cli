package com.arbetsformedlingen;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class StreamFactory {
    public static OutputStreamWriter createOutputStreamWriter(OutputStream os) {
        return new OutputStreamWriter(os, StandardCharsets.UTF_8);
    }

    public static BufferedInputStream createBufferedInputStream(InputStream is) {
        return new BufferedInputStream(is);
    }

    public static ByteArrayOutputStream createByteArrayOutputStream() {
        return new ByteArrayOutputStream();
    }
}
