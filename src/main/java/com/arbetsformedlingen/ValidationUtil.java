package com.arbetsformedlingen;

import com.jayway.jsonpath.JsonPath;

public class ValidationUtil {
    public static void validateServiceResponse(String serviceResponse, String expectClientId, String expectClientName) throws Exception {
        if(!expectClientId.equals(JsonPath.read(serviceResponse, "$.clientId"))) throw new Exception("Property 'clientId' does not match");
        if(!expectClientName.equals(JsonPath.read(serviceResponse, "$.name"))) throw new Exception("Property 'name' does not match");
        if(JsonPath.read(serviceResponse, "$.created") == null) throw new Exception("Property 'created' must not be null");
        if(JsonPath.read(serviceResponse, "$.updated") == null) throw new Exception("Property 'updated' must not be null");
    }

    public static void validateConsentResponse(String consentResponse, String expectClientId, String expectClientName) throws Exception {
        final var parsedConsentResponse = JsonPath.parse(consentResponse);
        if(!expectClientId.equals(parsedConsentResponse.read("$.clientId"))) throw new Exception("Property 'clientId' does not match");
        if(!expectClientName.equals(parsedConsentResponse.read("$.name"))) throw new Exception("Property 'name' does not match");
        // if(!parsedConsentResponse.read("$.enabled", Boolean.class)) throw new Exception("Property 'enabled' must be true");
        if(parsedConsentResponse.read("$.authCode") == null) throw new Exception("Property 'authCode' must not be null");
    }

    public static void validateRevokeResponse(String revokeResponse, String expectClientId, String expectClientName) throws Exception {
        final var parsedRevokeResponse = JsonPath.parse(revokeResponse);
        if(!expectClientId.equals(parsedRevokeResponse.read("$.clientId"))) throw new Exception("Property 'clientId' does not match");
        if(!expectClientName.equals(parsedRevokeResponse.read("$.name"))) throw new Exception("Property 'name' does not match");
        // if(parsedRevokeResponse.read("$.enabled", Boolean.class)) throw new Exception("Property 'enabled' be false");
        if(parsedRevokeResponse.read("$.authCode") != null) throw new Exception("Property 'authCode' must be null");
    }

    public static void validateStatusResponse(String statusResponse) throws Exception {
        final var parsedStatusResultContent = JsonPath.parse(statusResponse);
        if(!"Unemployed".equals(parsedStatusResultContent.read("$.status"))) throw new Exception("Property 'status' does not match 'Unemployed'");
    }
}
