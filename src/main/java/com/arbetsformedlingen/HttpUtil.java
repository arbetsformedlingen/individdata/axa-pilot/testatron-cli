package com.arbetsformedlingen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class HttpUtil {
    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    public static String request(String path, String method, List<Map<String, String>> headers, String encryptedContent) throws Exception {
        logger.debug("Assembling request to API...");
        final URL url = UrlFactory.createURL(path);
        final HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setRequestMethod(method);
        logger.debug("Request url: {}", url);

        httpCon.setRequestProperty("Content-Type", "application/json");
        logger.debug("Request content-type: {}", httpCon.getRequestProperty("Content-Type"));

        httpCon.setRequestProperty("User-Agent", "testatron-cli");

        for (var header : headers) {
            for (var key : header.keySet()) {
                httpCon.setRequestProperty(key, header.get(key));
                logger.debug("Request header[\"{}\"]: \"{}\"", key, httpCon.getRequestProperty(key));
            }
        }

        if (encryptedContent != null) {
            httpCon.setDoOutput(true);
            final String body = String.format("{\"encryptedContent\":\"%s\"}", encryptedContent);
            final OutputStream os = httpCon.getOutputStream();
            final OutputStreamWriter osw = StreamFactory.createOutputStreamWriter(os);
            osw.write(body);
            osw.flush();
            osw.close();
            os.close();
            logger.debug("Request body (length: {})", body.getBytes(StandardCharsets.UTF_8).length);
        }

        logger.info("Sending request to API... ({})", url);
        httpCon.connect();

        if (httpCon.getResponseCode() == 401) {
            logger.info("Received HTTP response code 401");
            httpCon.disconnect();
            throw new Exception("Response 401");
        }

        logger.debug("Parsing HTTP response body...");
        final BufferedInputStream bis = StreamFactory.createBufferedInputStream(httpCon.getInputStream());
        final ByteArrayOutputStream buf = StreamFactory.createByteArrayOutputStream();

        int result2 = bis.read();
        while(result2 != -1) {
            buf.write((byte) result2);
            result2 = bis.read();
        }

        final String body = buf.toString();
        logger.debug("Response body (length: {})", body.getBytes(StandardCharsets.UTF_8).length);

        httpCon.disconnect();

        logger.info("Parsed HTTP response");

        return body;
    }
}
