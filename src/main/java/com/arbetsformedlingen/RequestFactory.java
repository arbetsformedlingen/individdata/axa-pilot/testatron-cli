package com.arbetsformedlingen;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RequestFactory {
    public static String initRequest(String baseUrl,
                                     String clientName,
                                     RSAPrivateKey clientPrivateKey,
                                     RSAPublicKey afPublicKey,
                                     String clientId,
                                     String clientSecret,
                                     String foreignId,
                                     String pnr) throws Exception {
        return HttpUtil.request(
                baseUrl + "/externalAPI/init",
                "POST",
                new ArrayList<>(),
                MiscUtil.createEncryptedContent(
                        clientName,
                        clientPrivateKey,
                        afPublicKey,
                        List.of(
                                Map.of(
                                        "clientId", clientId,
                                        "clientSecret", clientSecret,
                                        "loginId", foreignId,
                                        "personalIdentityNumber", pnr
                                )
                        )
                )
        );
    }

    public static String tokenRequest(String baseUrl,
                                      String clientName,
                                      RSAPrivateKey clientPrivateKey,
                                      RSAPublicKey afPublicKey,
                                      String clientId,
                                      String clientSecret,
                                      String authCode) throws Exception {
        return HttpUtil.request(
                baseUrl + "/externalAPI/token",
                "POST",
                new ArrayList<>(),
                MiscUtil.createEncryptedContent(
                        clientName,
                        clientPrivateKey,
                        afPublicKey,
                        List.of(
                                Map.of(
                                        "clientId", clientId,
                                        "clientSecret", clientSecret,
                                        "authCode", authCode
                                )
                        )
                )
        );
    }

    public static String refreshRequest(String baseUrl,
                                        String clientName,
                                        RSAPrivateKey clientPrivateKey,
                                        RSAPublicKey afPublicKey,
                                        String clientId,
                                        String clientSecret,
                                        String refreshToken) throws Exception {
        return HttpUtil.request(
                baseUrl + "/externalAPI/refresh",
                "POST",
                new ArrayList<>(),
                MiscUtil.createEncryptedContent(
                        clientName,
                        clientPrivateKey,
                        afPublicKey,
                        List.of(
                                Map.of(
                                        "clientId", clientId,
                                        "clientSecret", clientSecret,
                                        "refreshToken", refreshToken
                                )
                        )
                )
        );
    }

    public static String statusRequest(String baseUrl,
                                       String clientName,
                                       RSAPrivateKey clientPrivateKey,
                                       RSAPublicKey afPublicKey,
                                       String accessToken) throws Exception {
        return HttpUtil.request(
                baseUrl + "/externalAPI/status",
                "POST",
                new ArrayList<>(),
                MiscUtil.createEncryptedContent(
                        clientName,
                        clientPrivateKey,
                        afPublicKey,
                        List.of(
                                Map.of(
                                        "accessToken", accessToken
                                )
                        )
                )
        );
    }

    public static String consentRequest(String baseUrl, String clientId, String scope, String foreignId, String pnr) throws Exception {
        return HttpUtil.request(
                String.format("%s/rest/user/giveConsent2?clientId=%s&scope=%s&axaId=%s",
                        baseUrl,
                        URLEncoder.encode(clientId, StandardCharsets.UTF_8),
                        URLEncoder.encode(scope, StandardCharsets.UTF_8),
                        URLEncoder.encode(foreignId, StandardCharsets.UTF_8)
                ),
                "GET",
                List.of(
                        Map.of(
                                "PISA_ID", pnr
                        )
                ),
                null);
    }

    public static String revokeRequest(String baseUrl, String clientId, String scope, String foreignId, String pnr) throws Exception {
        return HttpUtil.request(
                String.format("%s/rest/user/revokeConsent2?clientId=%s&scope=%s&axaId=%s",
                        baseUrl,
                        URLEncoder.encode(clientId, StandardCharsets.UTF_8),
                        URLEncoder.encode(scope, StandardCharsets.UTF_8),
                        URLEncoder.encode(foreignId, StandardCharsets.UTF_8)
                ),
                "GET",
                List.of(
                        Map.of(
                                "PISA_ID", pnr
                        )
                ),
                null);
    }

    public static String serviceRequest(String baseUrl, String clientId) throws Exception {
        return HttpUtil.request(
                String.format("%s/rest/service/%s/", baseUrl, URLEncoder.encode(clientId, StandardCharsets.UTF_8)),
                "GET",
                new ArrayList<>(),
                null
        );
    }
}
