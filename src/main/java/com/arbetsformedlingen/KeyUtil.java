package com.arbetsformedlingen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class KeyUtil {
    private static Logger logger = LoggerFactory.getLogger(KeyUtil.class);

    public static RSAPrivateKey getPrivateKey(File file) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        final Path path = file.toPath();
        final var absolutePath = path.toAbsolutePath();
        logger.debug("Loading private key: {}", absolutePath);
        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        final byte[] bytes = Files.readAllBytes(path);
        final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
        final RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        logger.info("Loaded private key: {}", absolutePath);
        return rsaPrivateKey;
    }

    public static RSAPublicKey getPublicKey(File file) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        final Path path = file.toPath();
        final var absolutePath = path.toAbsolutePath();
        logger.debug("Loading public key: {}", absolutePath);
        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        final byte[] bytes = Files.readAllBytes(path);
        final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        final RSAPublicKey rsaPublicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
        logger.info("Loaded public key: {}", absolutePath);
        return rsaPublicKey;
    }
}
